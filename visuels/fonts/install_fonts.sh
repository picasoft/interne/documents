# Extract fonts
for i in `ls *.zip`; do
	unar $i -o tmp | tee -a install.log
done

# Install fonts by moving all the .ttf and .odf
# files to ./local/share/fonts
fonts=`find  ./tmp -regex ".*.ttf\\|.*.otf"`

i=0
length=`echo "$fonts" | wc -l`
for font in $fonts; do
	let i++
	echo -n "($i/$length) moving $font..." | tee -a install.log
	if (mv $font -t ~/.local/share/fonts); then
		echo "done" | tee -a install.log
	else
		echo -e "\nmoving $font failed!" | tee -a install.log
		break
	fi
done

rm -rf tmp
