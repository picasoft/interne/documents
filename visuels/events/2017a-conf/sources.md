###affiche1_conf_a17
Textes originaux:

>Anytime someone puts a lock on something that belongs to you and won’t give you the key, it’s not there for your benefit.  
[ifixit.org](https://ifixit.org/blog/9236/day-against-drm/)

>Digital Rights Management (DRM) technologies attempt to control what you can and can't do with the media and hardware you've purchased.

>    Bought an ebook from Amazon but can't read it on your ebook reader of choice? That's DRM.  
>    Bought a video game but can't play it today because the manufacturer's "authentication servers" are offline? That's DRM.  
>    Bought a smartphone but can't use the applications or the service provider you want on it? That's DRM.  
>    Bought a DVD or Blu-Ray but can't copy the video onto your portable media player? That's DRM.  

>Corporations claim that DRM is necessary to fight copyright infringement online and keep consumers safe from viruses. But there's no evidence that DRM helps fight either of those. Instead DRM helps big business stifle innovation and competition by making it easy to quash "unauthorized" uses of media and technology.":  
[eff.org](https://www.eff.org/issues/drm)

Image menottes: Au début je pensais qu'elle avait été faite par Ifixit, mais ensuite je l'ai retrouvée sur [openclipart.org](https://openclipart.org/detail/271469/drm-handcuffs-word-cloud-black)

-----
###affiche2_conf_a17
	https://static.fsf.org/nosvn/dbd/2012/day-against-drm/image2.png
