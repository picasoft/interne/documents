# Ré-utiliser les logos

Voir [le wiki](https://wiki.picasoft.net/doku.php?id=asso:communication:reutiliser_logo).

Il est nécessaire d'installer les polices [MontSerrat Alternatives](https://fontlibrary.org/en/font/montserrat-alternates) et [Comfortaa](https://fontlibrary.org/en/font/comfortaa). La méthode d'installation dépend de votre système d'exploitation, n'hésitez pas à demander de l'aide.
