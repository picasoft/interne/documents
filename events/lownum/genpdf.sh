#!/bin/bash
for f in *.svg; 
do echo $f; 
inkscape "$f" -o "$f".pdf; 
done;
pdfunite *.svg.pdf lownum-fds.pdf;

for f in simple/*.svg; 
do echo $f; 
inkscape "$f" -o "$f".pdf; 
done;
pdfunite simple/*.svg.pdf ./lownum-fds_simple.pdf;
