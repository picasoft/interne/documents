#!/bin/bash
# Génére plusieurs présentations prêtes à l'emploi à partir de "fds-pres.pdf"

# Présentation enfants en 2 slides 2'
pdftk fds-pres.pdf cat 1 6 output fds-pres-primaire.pdf

# Présentation collégiens en 3 slides 3'
pdftk fds-pres.pdf cat 8 1 7 output fds-pres-college01.pdf
pdftk fds-pres.pdf cat 9 1 7 output fds-pres-college02.pdf
pdftk fds-pres.pdf cat 10 1 7 output fds-pres-college03.pdf
pdftk fds-pres.pdf cat 11 1 7 output fds-pres-college04.pdf
pdftk fds-pres.pdf cat 12 1 7 output fds-pres-college05.pdf

# Présentation générale en 5 slides 5' (4 5 et 6 7 sont les mêmes slides)
pdftk fds-pres.pdf cat 1 2 4 5 6 7 13 output fds-pres-courte.pdf

# Export de chaque page en PNG
pdftk fds-pres.pdf cat 1 output 1.pdf
for i in `seq 1 13`
do
	pdftk fds-pres.pdf cat $i output $i.pdf
	inkscape $i.pdf -z --export-dpi=150  --export-png=$i.png
	rm $i.pdf
done

