Dépôt créé pour capitaliser tous les documents publics de Picasoft (com, statuts...).

**Seuls les documents publics sont déposés ici.**

Tous les documents déposés ici sont associés à une license cc-by-sa.

## Notice d'export PDF

Les fichiers _mail.pdf ou _web.pdf sont des fichiers légers destinés à l'envoi de mail ou l'affichage écran. Ils sont obtenus avec LibreOffice Draw en choisissant les options `Compression JPEG 50% et Réduire la résolution des images 150 DPI`.

Les fichiers _print.pdf sont des fichiers lourds destinés à l'impression. Ils sont obtenus avec LibreOffice Draw en choisissant l'option `Compression JPEG 80%` (sans réduction de la résolution des images).
